import Vue from 'vue'
import Router from 'vue-router'

import HelloWorld from '@/components/Hello'
import Login from '@/components/Login'
import Register from '@/components/Register'
import NewReferral from '@/components/referrals/New'
import Referrals from '@/components/referrals/View'

import firebase from 'firebase'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/',
      name: 'Hello',
      component: HelloWorld,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/referrals',
      name: 'Referrals',
      component: Referrals,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/new-referral',
      name: 'NewReferral',
      component: NewReferral,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  let user = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !user) next('login')
  else if (!requiresAuth && user) next('/')
  else next()
})

export default router
